# channel

## Getting Started

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/bin/pserve development.ini


## Running

For testing purposes, I have not set user and password for the database.

I use docker to develop my softwares and I used a image of mongo (mvertes/alpine-mongo:latest).
But on development.ini have that url to connect to mongo database.


`- mongo_uri = mongodb://@localhost:27017/channel`

So, you just can use MongoDB server on your machine.

On `channel/tests.py` I have set on code a different `mongo_uri` to Gitlab CI

`- mongo_uri = mongodb://@mongo:27017/channel`
