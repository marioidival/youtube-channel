from pyramid.view import view_config
from pyramid.httpexceptions import exception_response, HTTPCreated
from bson.objectid import ObjectId
from bson.son import SON


@view_config(route_name='home', renderer='templates/home.jinja2')
def home_view(request):
    return {'videos': request.db.videos.find()}


@view_config(route_name='themes', renderer='templates/themes.jinja2')
def themes_view(request):
    pipeline = [
        {
            '$group': {
                '_id': '$videoTheme',
                'score': {
                    '$sum': {
                        '$subtract': [
                            '$thumbs_up', {'$divide': ['$thumbs_down', 2]}
                        ]
                    }
                }
            },
        },
        {
            '$sort': SON([('score', -1)]),
        }
    ]
    result = request.db.videos.aggregate(pipeline)
    return {'themes': result}


@view_config(route_name='save_video', renderer='json', request_method=['POST'])
def create_video_view(request):
    new_video = request.json
    # find a video with same name and return conflict
    founded = request.db.videos.find(new_video)
    if founded.count() >= 1:
        raise exception_response(409)

    new_video.update({'thumbs_up': 0, 'thumbs_down': 0})
    # save and return created
    request.db.videos.insert(new_video)
    return HTTPCreated()


@view_config(route_name='thumbs_up_video', renderer='json')
def thumbsup(request):
    return vote_video(request, 'thumbs_up')


@view_config(route_name='thumbs_down_video', renderer='json')
def thumbsdown(request):
    return vote_video(request, 'thumbs_down')


def vote_video(request, action):
    video_id = ObjectId(request.matchdict['id'])
    search = {'_id': video_id}

    request.db.videos.update(
        search,
        {'$inc': {action: 1}}
    )
    current_video = request.db.videos.find_one(search)
    return {action: current_video[action]}
