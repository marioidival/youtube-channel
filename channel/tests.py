import unittest
from urllib.parse import urlparse

from pyramid import testing
from pymongo import MongoClient


class FunctionalTests(unittest.TestCase):
    settings = {'mongo_uri': 'mongodb://@mongo:27017/channel_test'}
    def setUp(self):
        from channel import main
        app = main({}, **self.settings)

        from webtest import TestApp
        self.testapp = TestApp(app)

    def tearDown(self):
        url = urlparse(self.settings['mongo_uri'])
        client = MongoClient(host=url.hostname, port=url.port)
        client.drop_database('channel_test')

    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'Channel' in res.body)

    def test_create_video(self):
        body = {'videoName': 'test', 'videoTheme': 'test theme'}
        res = self.testapp.post_json('/videos/new/', body)
        self.assertEquals(201, res.status_code)

    def test_create_video_twice(self):
        body = {'videoName': 'test twice', 'videoTheme': 'test theme'}
        res = self.testapp.post_json('/videos/new/', body)
        self.assertEquals(201, res.status_code)

        res = self.testapp.post_json('/videos/new/', body, status=409)
        self.assertEquals(409, res.status_code)

    def test_thumbs_up_video(self):
        client = self.testapp.app.registry.db['channel_test']
        # just create some video
        body = {'videoName': 'test twice', 'videoTheme': 'test theme'}
        res = self.testapp.post_json('/videos/new/', body)

        ids = [video['_id'] for video in client.videos.find()]

        for count, _id in enumerate(ids):
            res = self.testapp.post_json(f'/videos/{_id}/up')
            self.assertEquals(1, res.json['thumbs_up'])

            res = self.testapp.post_json(f'/videos/{_id}/up')
            self.assertEquals(2, res.json['thumbs_up'])

            res = self.testapp.post_json(f'/videos/{_id}/up')
            self.assertEquals(3, res.json['thumbs_up'])

    def test_thumbs_down_video(self):
        client = self.testapp.app.registry.db['channel_test']
        # just create some video
        body = {'videoName': 'test twice', 'videoTheme': 'test theme'}
        res = self.testapp.post_json('/videos/new/', body)

        ids = [video['_id'] for video in client.videos.find()]

        for count, _id in enumerate(ids):
            res = self.testapp.post_json(f'/videos/{_id}/down')
            self.assertEquals(1, res.json['thumbs_down'])

            res = self.testapp.post_json(f'/videos/{_id}/down')
            self.assertEquals(2, res.json['thumbs_down'])

            res = self.testapp.post_json(f'/videos/{_id}/down')
            self.assertEquals(3, res.json['thumbs_down'])
