from urllib.parse import urlparse
from pyramid.config import Configurator
from pymongo import MongoClient


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)

    mongo_url = urlparse(settings.get('mongo_uri'))
    config.registry.db = MongoClient(
        host=mongo_url.hostname,
        port=mongo_url.port,
    )

    def add_db(request):
        '''Get current db and return MongoClient instance'''
        db = config.registry.db[mongo_url.path[1:]]

        if mongo_url.username and mongo_url.password:
            db.authenticate(mongo_url.username, mongo_url.password)
        return db

    config.add_request_method(add_db, 'db', reify=True)

    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static', cache_max_age=3600)

    # routers
    config.add_route('home', '/')
    config.add_route('save_video', '/videos/new/')
    config.add_route('thumbs_up_video', '/videos/{id}/up')
    config.add_route('thumbs_down_video', '/videos/{id}/down')
    config.add_route('themes', '/themes')
    config.scan()

    return config.make_wsgi_app()
